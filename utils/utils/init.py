import ast
import glob
import logging
import os
import re

log = logging.getLogger(__name__)


def __init__(hub):
    # Load up the utils app config and data
    hub.pop.config.load(["utils"], cli="utils")
    hub.utils.UTILITY = hub.OPT.utils.get("utility", "")

    # Find the app name from the base_dir
    hub.utils.PATH = os.path.abspath(hub.OPT.utils.base_dir)
    hub.utils.BASE = os.path.split(hub.utils.PATH)[-1]
    hub.utils.APP = _get_app(hub.utils.BASE)
    # Dynamically collect the dynamic names used in the named app
    hub.utils.DYNES = {
        dyne
        for dyne in hub._dynamic
        if any(hub.utils.PATH in path for path in hub._dynamic[dyne]["paths"])
    }
    print(hub.utils.DYNES)
    # Vertically merge the same dynes
    for dyne in hub.utils.DYNES:
        if hub.utils.APP == dyne:
            continue
        hub.pop.sub.add(dyne_name=dyne)

    # Horizontally merge the main app
    log.debug(f"app-merging {hub.utils.APP} from {os.path.abspath(hub.utils.BASE)}")
    try:
        hub.pop.sub.add(hub.utils.APP)
    except ModuleNotFoundError:
        raise ModuleNotFoundError(
            f"'{hub.utils.APP}' cannot be found, is it pip installed into this environment?"
        )

    # Load the specified dyne names
    for dyne in hub.OPT["utils"].get("dyne"):
        log.debug(f"loading dyne: {dyne}")
        hub.pop.sub.add(dyne_name=dyne)


def run(hub):
    if hub.utils.UTILITY == "create-tests":
        hub.pop.loop.start(hub.utils.test.create())
    elif hub.utils.UTILITY == "update-tests":
        hub.pop.loop.start(hub.utils.test.update())
    else:
        raise ValueError(f"Unknown utility: {hub.utils.UTILITY}")


def _get_app(base_dir: str) -> str:
    """
    Find the name of the pop app in the given directory
    """
    abspath = os.path.abspath(base_dir)

    run_ = os.path.join(abspath, "run.py")
    if os.path.exists(run_):
        log.debug("Trying to get the app name from run.py")
        pattern = re.compile(
            "hub\.pop\.sub\.add\((dyne_name\s*=\s*)?[\"'](\w+)\.?(\w+)?[\"']\)"
        )
        with open(run_, "r") as fh:
            match = pattern.findall(fh.read(), re.MULTILINE)
            for m in match:
                if m[1] in os.path.split(abspath):
                    return m[1]
            if match:
                return match[0][1]

    for conf in glob.glob(os.path.join(abspath, "*", "conf.py"), recursive=True):
        log.debug(f"Trying to get the app name from {conf}")
        dyne = {}
        with open(conf, "r") as fh:
            start = False
            for line in fh.readlines():
                if "DYNE" in line:
                    start = True
                elif start and "}" in line:
                    break
                elif start:
                    dyne.update(ast.literal_eval("{%s}" % line))
                else:
                    pass
        for k, v in dyne.items():
            if k in os.path.split(abspath):
                return k

    if os.path.isdir(abspath):
        log.debug("Assuming the app name is the last name in the given path")
        return os.path.split(abspath)[-1]

    log.debug("Assuming the app name is the parent of the file in this path")
    return os.path.split(os.path.dirname(abspath))[-1]
