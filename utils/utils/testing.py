import aiofiles
import logging
import os
import pop.contract as contract
from typing import List, Tuple

__virtualname__ = "test"
log = logging.getLogger(__name__)


async def create_test_dirs(hub, *sub_dirs: List[str]):
    test_base = os.path.join(hub.utils.PATH, "tests")
    log.debug(f"Creating test structure under root: {test_base}")
    await hub.utils.test.makedirs(test_base, exist_ok=True)

    ret = []
    for dir_ in sub_dirs:
        sub_dir_path = os.path.join(test_base, dir_)
        ret.append(sub_dir_path)
        if not os.path.isdir(sub_dir_path):
            log.debug(f"Creating directory {sub_dir_path}")
            await hub.utils.test.makedirs(sub_dir_path)
    return ret


def _test_func_name(func) -> Tuple[str, bool]:
    # This is the name nof the test function
    if isinstance(func, contract.ContractedAsync):
        return f"async def test_{func.__name__}", True
    else:
        return f"def test_{func.__name__}", False


TEST_CLASS = '''# import {module}
import pytest


class Test{class_name}:
    """
    Automatically generated test
    """
'''

TEST_FUNC = """
    {mark_async}
    {func}(self, hub):
        mock_hub = hub.pop.testing.mock_hub()
"""

TEST_CONF = '''import pop.hub
import pytest
import sys
import unittest.mock as mock


@pytest.fixture(scope="session")
def hub():
    """
    provides a full hub that is used as a reference for mock_hub
    """
    hub = pop.hub.Hub()

    # strip pytest args
    with mock.patch.object(sys, "argv", sys.argv[:1]):
{dynes}

    return hub
'''


async def makedirs(hub, path: str, exist_ok: bool = False):
    builder = ""
    for dir_ in os.path.split(path):
        builder = os.path.join(builder, dir_)
        if not os.path.exists(builder):
            os.makedirs(path, exist_ok=exist_ok)
            init_file = os.path.join(builder, "__init__.py")
            async with aiofiles.open(init_file, "w+") as fh_:
                pass


async def create(hub):
    funcs = await hub.utils.test.read_subs()
    paths = await hub.utils.test.create_test_dirs("unit", "integration")
    conf_test_path = os.path.join(hub.utils.PATH, "tests", "conftest.py")
    sub_add = "\n".join(
        f'        hub.pop.sub.add(dyne_name="{d}")'
        for d in sorted(hub.utils.DYNES)
        if hub.utils.APP != d
    )
    if not os.path.exists(conf_test_path):
        with open(conf_test_path, "w+") as fh_:
            fh_.write(TEST_CONF.format(dynes=sub_add if sub_add else "pass"))

    # Make the subdirectories based on functions that exist
    test_files = set()
    for path in paths:
        for func in funcs:
            mod = func.func.__module__.split(hub.utils.APP)
            if len(mod) > 1:
                file_path = os.path.join(path, *mod[-1].split(".")) + ".py"
                dir_name = os.path.dirname(file_path)
                file_path = os.path.join(
                    dir_name, "test_" + os.path.split(file_path)[-1]
                )
                test_files.add(file_path)
                await hub.utils.test.makedirs(dir_name, exist_ok=True)
                # Create an init file in this directory so that pytest doesn't freak out
                init_file = os.path.join(os.path.dirname(file_path), "__init__.py")
                async with aiofiles.open(init_file, "w+"):
                    pass
                needs_class = True
                needs_test = True
                test_func, is_async = _test_func_name(func)
                if os.path.exists(file_path):
                    with open(file_path, "r+") as fh_:
                        text = fh_.read()
                        if "Test" in text:
                            needs_class = False
                        if test_func in text:
                            needs_test = False

                if needs_class:
                    module = mod[-1][1:]
                    class_name = module.split(".")[-1].title().replace("_", "")
                    with open(file_path, "a") as fh_:
                        fh_.write(
                            TEST_CLASS.format(module=module, class_name=class_name)
                        )
                if needs_test:
                    with open(file_path, "a") as fh_:
                        fh_.write(
                            TEST_FUNC.format(
                                mark_async="@pytest.mark.asyncio" if is_async else "",
                                func=test_func,
                            )
                        )

    # the function args
    # pprint(func.signature)

    # These are the names of functions called within that code
    # pprint(func.func.__code__.co_names)


async def read_subs(hub) -> List[contract.Contracted]:
    ret = []
    for mod in hub._iter_subs:
        ret.extend(await hub.utils.test.get_funcs(getattr(hub, mod)))
        for sub in hub.pop.sub.iter_subs(getattr(hub, mod), recurse=True):
            ret.extend(await hub.utils.test.get_funcs(sub))

    return ret


async def get_funcs(hub, sub) -> List[contract.Contracted]:
    """
    Get the contracted functions from inside a sub
    """
    ret = []
    for mod in sub:
        for func in mod:
            ret.append(func)
    return ret


async def update(hub):
    """TODO: if tests don't exist then call create"""
