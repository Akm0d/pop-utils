CLI_CONFIG = {
    "utility": {"display_priority": 1, "positional": True,},
    "base_dir": {"display_priority": 2, "positional": True,},
    "dyne": {"action": "append", "options": ["-d", "--dyne"],},
}
CONFIG = {
    "utility": {"help": "The utility to call", "default": "", "type": str,},
    "base_dir": {
        "help": "The root directory of the pop-app to use",
        "default": "",
        "type": str,
    },
    "dyne": {"default": [], "help": "The Dyne name of a sub", "type": str,},
}
DYNE = {
    "utils": ["utils"],
}
